defmodule Blog.Accounts.QueriesTest do
  use Blog.DataCase

  import Blog.AccountsFixtures

  alias Blog.Accounts
  alias Blog.Schemas

  describe "get_user!/1" do
    test "raises if id is invalid" do
      assert_raise Ecto.NoResultsError, fn ->
        Accounts.Queries.get_user!("11111111-1111-1111-1111-111111111111")
      end
    end

    test "returns the user with the given id" do
      %{id: id} = user = user_fixture()
      assert %Schemas.User{id: ^id} = Accounts.Queries.get_user!(user.id)
    end
  end

  describe "get_user_by_email/1" do
    test "does not return the user if the email does not exist" do
      refute Accounts.Queries.get_user_by_email("unknown@example.com")
    end

    test "returns the user if the email exists" do
      %{id: id} = user = user_fixture()
      assert %Schemas.User{id: ^id} = Accounts.Queries.get_user_by_email(user.email)
    end
  end

  describe "get_user_by_email_and_password/2" do
    test "does not return the user if the email does not exist" do
      refute Accounts.Queries.get_user_by_email_and_password("unknown@example.com", "hello world!")
    end

    test "does not return the user if the password is not valid" do
      user = user_fixture()
      refute Accounts.Queries.get_user_by_email_and_password(user.email, "invalid")
    end

    test "returns the user if the email and password are valid" do
      %{id: id} = user = user_fixture()

      assert %Schemas.User{id: ^id} =
               Accounts.Queries.get_user_by_email_and_password(user.email, valid_user_password())
    end
  end

  describe "get_user_by_session_token/1" do
    setup do
      user = user_fixture()
      token = Accounts.generate_user_session_token(user)
      %{user: user, token: token}
    end

    test "returns user by token", %{user: user, token: token} do
      assert session_user = Accounts.Queries.get_user_by_session_token(token)
      assert session_user.id == user.id
    end

    test "does not return user for invalid token" do
      refute Accounts.Queries.get_user_by_session_token("oops")
    end

    test "does not return user for expired token", %{token: token} do
      {1, nil} = Repo.update_all(Schemas.UserToken, set: [inserted_at: ~N[2020-01-01 00:00:00]])
      refute Accounts.Queries.get_user_by_session_token(token)
    end
  end

  describe "get_user_by_reset_password_token/1" do
    setup do
      user = user_fixture()

      token =
        extract_user_token(fn url ->
          Accounts.deliver_user_reset_password_instructions(user, url)
        end)

      %{user: user, token: token}
    end

    test "returns the user with valid token", %{user: %{id: id}, token: token} do
      assert %Schemas.User{id: ^id} = Accounts.Queries.get_user_by_reset_password_token(token)
      assert Repo.get_by(Schemas.UserToken, user_id: id)
    end

    test "does not return the user with invalid token", %{user: user} do
      refute Accounts.Queries.get_user_by_reset_password_token("oops")
      assert Repo.get_by(Schemas.UserToken, user_id: user.id)
    end

    test "does not return the user if token expired", %{user: user, token: token} do
      {1, nil} = Repo.update_all(Schemas.UserToken, set: [inserted_at: ~N[2020-01-01 00:00:00]])
      refute Accounts.Queries.get_user_by_reset_password_token(token)
      assert Repo.get_by(Schemas.UserToken, user_id: user.id)
    end
  end
end
