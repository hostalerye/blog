defmodule Blog.Repo.Migrations.CreatePostsTable do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :string, null: false
      add :content, :text, null: false
      add :published, :boolean, default: false

      add :author_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
