defmodule BlogWeb.PageController do
  use BlogWeb, :controller

  alias Blog.Posts
  alias Blog.Repo
  alias Blog.Schemas

  def home(conn, _params) do
    posts =
      Schemas.Post
      |> Posts.Queries.by_published(true)
      |> Posts.Queries.preload_author()
      |> Repo.all()

    render(conn, :home, posts: posts)
  end
end
