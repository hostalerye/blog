defmodule BlogWeb.PostLive.Index do
  @moduledoc false
  use BlogWeb, :live_view

  alias Blog.Posts
  alias Blog.Repo
  alias Blog.Schemas

  @impl true
  def mount(_params, _session, socket) do
    posts =
      Schemas.Post
      |> Posts.Queries.by_author_id(socket.assigns.current_user.id)
      |> Repo.all()

    {:ok, stream(socket, :posts, posts)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    post =
      Schemas.Post
      |> Posts.Queries.by_id(id)
      |> Repo.one!()

    socket
    |> assign(:page_title, "Edit Post")
    |> assign(:post, post)
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Post")
    |> assign(:post, %Schemas.Post{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Posts")
    |> assign(:post, nil)
  end

  @impl true
  def handle_info({BlogWeb.PostLive.FormComponent, {:saved, post}}, socket) do
    {:noreply, stream_insert(socket, :posts, post)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    post =
      Schemas.Post
      |> Posts.Queries.by_id(id)
      |> Repo.one!()

    {:ok, _} = Posts.delete_post(post)

    {:noreply, stream_delete(socket, :posts, post)}
  end
end
