<.header>
  <%= @post.title %>
  <:actions :if={@is_mine}>
    <.link patch={~p"/posts/#{@post}/show/edit"} phx-click={JS.push_focus()}>
      <.button>Edit post</.button>
    </.link>
    <.link phx-click={JS.push("toggle_published")}>
      <%= if @post.published do %>
        <.button>Unpublish</.button>
      <% else %>
        <.button>Publish</.button>
      <% end %>
    </.link>
  </:actions>
</.header>

<div><%= @post.content %></div>

<div class="py-3">
  <h3 class="text-lg font-semibold leading-8 text-zinc-800">Comments</h3>
  <%= for {id, comment} <- @streams.comments do %>
    <div id={id} class="mb-2 border-b">
      <p class="text-right text-sm">
        By <%= comment.author.first_name %> <%= comment.author.last_name %>
      </p>
      <p><%= comment.content %></p>
    </div>
  <% end %>

  <.simple_form for={@comment_form} id="comment-form" phx-submit="save_comment">
    <.input field={@comment_form[:content]} type="textarea" label="Add a comment" required />

    <:actions>
      <.button phx-disable-with="Saving...">Add comment</.button>
    </:actions>
  </.simple_form>
</div>

<.back navigate={~p"/posts"}>Back to posts</.back>

<.modal :if={@live_action == :edit} id="post-modal" show on_cancel={JS.patch(~p"/posts/#{@post}")}>
  <.live_component
    module={BlogWeb.PostLive.FormComponent}
    id={@post.id}
    title={@page_title}
    action={@live_action}
    post={@post}
    patch={~p"/posts/#{@post}"}
  />
</.modal>
