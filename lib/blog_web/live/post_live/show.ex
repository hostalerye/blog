defmodule BlogWeb.PostLive.Show do
  @moduledoc false
  use BlogWeb, :live_view

  alias Blog.Posts
  alias Blog.Repo
  alias Blog.Schemas

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    post =
      Schemas.Post
      |> Posts.Queries.by_id(id)
      |> Repo.one!()

    comments =
      Schemas.Comment
      |> Posts.Queries.by_post_id(post.id)
      |> Posts.Queries.preload_author()
      |> Repo.all()

    is_mine = socket.assigns.current_user && post.author_id == socket.assigns.current_user.id

    comment_form = to_form(%{"content" => ""}, as: "comment")

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:post, post)
     |> stream(:comments, comments)
     |> assign(:comment_form, comment_form)
     |> assign(:is_mine, is_mine)}
  end

  @impl true
  def handle_event("toggle_published", _, socket) do
    {:ok, post} = Posts.toggle_published(socket.assigns.post)

    info = if post.published, do: "Post published", else: "Post unpublished"

    {:noreply,
     socket
     |> put_flash(:info, info)
     |> assign(:post, post)}
  end

  @impl true
  def handle_event("save_comment", %{"comment" => comment_params}, socket) do
    {:ok, _comment} = Posts.create_comment(comment_params, socket.assigns.post, socket.assigns.current_user)

    comments =
      Schemas.Comment
      |> Posts.Queries.by_post_id(socket.assigns.post.id)
      |> Posts.Queries.preload_author()
      |> Repo.all()

    {:noreply,
     socket
     |> put_flash(:info, "Comment added successfully")
     |> stream(:comments, comments)}
  end

  defp page_title(:show), do: "Show Post"
  defp page_title(:edit), do: "Edit Post"
end
