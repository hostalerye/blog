defmodule Blog.Posts do
  @moduledoc false

  alias Blog.Posts
  alias Blog.Repo
  alias Blog.Schemas

  def create_post(attrs, %Schemas.User{} = user) do
    %Schemas.Post{author_id: user.id}
    |> Posts.Post.creation_changeset(attrs)
    |> Repo.insert()
  end

  def toggle_published(%Schemas.Post{} = post) do
    post
    |> Posts.Post.publication_changeset(%{published: !post.published})
    |> Repo.update()
  end

  def delete_post(%Schemas.Post{} = post) do
    Repo.delete(post)
  end

  def change_post(%Schemas.Post{} = post, attrs \\ %{}) do
    Posts.Post.update_changeset(post, attrs)
  end

  def update_post(%Schemas.Post{} = post, attrs) do
    post
    |> Posts.Post.update_changeset(attrs)
    |> Repo.update()
  end

  def create_comment(attrs, %Schemas.Post{} = post, %Schemas.User{} = user) do
    %Schemas.Comment{author_id: user.id, post_id: post.id}
    |> Posts.Comment.creation_changeset(attrs)
    |> Repo.insert()
  end
end
