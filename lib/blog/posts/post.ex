defmodule Blog.Posts.Post do
  @moduledoc false
  import Ecto.Changeset

  alias Blog.Schemas

  def creation_changeset(%Schemas.Post{} = post, attrs) do
    post
    |> cast(attrs, [:title, :content, :published, :author_id])
    |> validate_required([:title, :content, :published, :author_id])
    |> foreign_key_constraint(:author_id)
  end

  def update_changeset(%Schemas.Post{} = post, attrs) do
    post
    |> cast(attrs, [:title, :content])
    |> validate_required([:title, :content])
  end

  def publication_changeset(%Schemas.Post{} = post, attrs) do
    post
    |> cast(attrs, [:published])
    |> validate_required([:published])
  end
end
