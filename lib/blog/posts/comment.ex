defmodule Blog.Posts.Comment do
  @moduledoc false
  import Ecto.Changeset

  alias Blog.Schemas

  def creation_changeset(%Schemas.Comment{} = post, attrs) do
    post
    |> cast(attrs, [:content, :author_id, :post_id])
    |> validate_required([:content, :author_id, :post_id])
    |> foreign_key_constraint(:author_id)
    |> foreign_key_constraint(:post_id)
  end
end
