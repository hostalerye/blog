defmodule Blog.Posts.Queries do
  @moduledoc false
  import Ecto.Query

  def by_published(query, published) do
    where(query, published: ^published)
  end

  def by_author_id(query, author_id) do
    where(query, author_id: ^author_id)
  end

  def by_id(query, id) do
    where(query, id: ^id)
  end

  def by_post_id(query, post_id) do
    where(query, post_id: ^post_id)
  end

  def preload_author(query) do
    query
    |> join(:inner, [p], a in assoc(p, :author))
    |> preload(:author)
  end
end
