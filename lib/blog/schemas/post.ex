defmodule Blog.Schemas.Post do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "posts" do
    field :title, :string
    field :content, :string
    field :published, :boolean, default: true

    belongs_to :author, Blog.Schemas.User

    has_many :comments, Blog.Schemas.Comment

    timestamps()
  end
end
