defmodule Blog.Schemas.Comment do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "comments" do
    field :content, :string

    belongs_to :author, Blog.Schemas.User
    belongs_to :post, Blog.Schemas.Post

    timestamps()
  end
end
