defmodule Blog.Accounts.Queries do
  @moduledoc false
  alias Blog.Accounts
  alias Blog.Repo
  alias Blog.Schemas

  def get_user!(id), do: Repo.get!(Schemas.User, id)

  @doc """
  Gets a user by email.

  ## Examples

      iex> get_user_by_email("foo@example.com")
      %Schemas.User{}

      iex> get_user_by_email("unknown@example.com")
      nil

  """
  def get_user_by_email(email) when is_binary(email) do
    Repo.get_by(Schemas.User, email: email)
  end

  @doc """
  Gets a user by email and password.

  ## Examples

      iex> get_user_by_email_and_password("foo@example.com", "correct_password")
      %Schemas.User{}

      iex> get_user_by_email_and_password("foo@example.com", "invalid_password")
      nil

  """
  def get_user_by_email_and_password(email, password) when is_binary(email) and is_binary(password) do
    user = Repo.get_by(Schemas.User, email: email)
    if Accounts.User.valid_password?(user, password), do: user
  end

  @doc """
  Gets the user with the given signed token.
  """
  def get_user_by_session_token(token) do
    {:ok, query} = Accounts.UserToken.verify_session_token_query(token)
    Repo.one(query)
  end

  @doc """
  Gets the user by reset password token.

  ## Examples

      iex> get_user_by_reset_password_token("validtoken")
      %Schemas.User{}

      iex> get_user_by_reset_password_token("invalidtoken")
      nil

  """
  def get_user_by_reset_password_token(token) do
    with {:ok, query} <- Accounts.UserToken.verify_email_token_query(token, "reset_password"),
         %Schemas.User{} = user <- Repo.one(query) do
      user
    else
      _ -> nil
    end
  end
end
